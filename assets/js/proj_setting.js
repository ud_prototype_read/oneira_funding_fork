var TIAA_ud = TIAA_ud || {};

TIAA_ud = {
	udThemeUrl			: '/themes/ud_atom/release_2015-09/',
	globalAssetUrl		: '/assets/',
	globalThemeUrl		: '/assets/release_2015-09/',
	localUrl			: 'assets/',
	leftNavMode			: 'none', // - For Participant Summary like pages ps' || DEFAULT is 'none' - No Left nav || '$Element' - The section that needs to be on right side,
	oneColWidth			: 1080,
	twoColWidth			: 980,
	versioning			: true,	
	// phpMode			: true,   		// Use PHP
	phpMode				: false,   		// Use jQuery
	scenarioCheck		: 'release', 	// For version specific scenario - enter release value
	panelToggleVisible	: true, 		// Panel toggling
	startVersion		: 'sprint39',  		// Starting version
	cssName				: 'ud_proj.css',				// Proj Specific CSS	
	jsName				: 'ud_proj.js',					// Proj Specific JS
	projLoc				: '/ud/oneira_funding/'		// Location of currect project from prototype root
};
 
// This is collective object data for Unified desktop
TIAA_ud.releaseObj = {
	// DO NOT EDIT THE BELOW

	// Current default codes
	defaults: {
		proCSS: true,
		left: true,
		header: true,  
		pagetitle: true,
		body: true,
		popups: true,
		proJS: true
	},
	// DO NOT EDIT THE ABOVE CODE
 
	// FEEL FREE TO UPDATE THE BELOW CODE
	sprint6: {
		themeUrl:{
			udThemeUrl			: '/themes/ud2_atom/release_2016-03/',
			globalThemeUrl		: '/assets/release_2016-03/'
		},
		add_funding: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		fund_an_account: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_detail: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		}
	},
	sprint7: {
		themeUrl:{
			udThemeUrl			: '/themes/ud2_atom/release_2016-03/',
			globalThemeUrl		: '/assets/release_2016-03/'
		},
		add_funding: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		fund_an_account: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_detail: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		}
	},
	sprint8: {
		themeUrl:{
			udThemeUrl			: '/themes/ud2_atom/release_2016-03/',
			globalThemeUrl		: '/assets/release_2016-03/'
		},
		add_funding: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		fund_an_account: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_detail: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		}
	},
	sprint9: {
		themeUrl:{
			udThemeUrl			: '/themes/ud2_atom/release_2016-03/',
			globalThemeUrl		: '/assets/release_2016-03/'
		},
		add_funding: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		fund_an_account: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_detail: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_igonigo: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_lao: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		}
	},
	sprint10: {
		themeUrl:{
			udThemeUrl			: '/themes/ud2_atom/release_2016-03/',
			globalThemeUrl		: '/assets/release_2016-03/'
		},
		add_funding: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		fund_an_account: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_detail: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_igonigo: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_lao: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		}
	},
	sprint11: {
		themeUrl:{
			udThemeUrl			: '/themes/ud2_atom/release_2016-07/',
			globalThemeUrl		: '/assets/release_2016-07/'
		},
		add_funding: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		fund_an_account: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_detail: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_igonigo: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_lao: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		docusign_confirmation: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		}
	},
	sprint12: {
		themeUrl:{
			udThemeUrl			: '/themes/ud2_atom/release_2016-07/',
			globalThemeUrl		: '/assets/release_2016-07/'
		},
		add_funding: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		fund_an_account: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_detail: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_igonigo: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_lao: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		docusign_confirmation: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		internal_funding_detail: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		msg_nigo_before: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		msg_nigo_after: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		}
	},
	sprint13: {
		themeUrl:{
			udThemeUrl			: '/themes/ud2_atom/release_2016-07/',
			globalThemeUrl		: '/assets/release_2016-07/'
		},
		add_funding: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		fund_an_account: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_detail: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_igonigo: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_lao: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		docusign_confirmation: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		internal_funding_detail: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		msg_nigo_before: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		msg_nigo_after: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		}
	},
	sprint14: {
		themeUrl:{
			udThemeUrl: 		'/themes/ud2_atom/release_2016-07/',
			globalThemeUrl: '/assets/release_2016-07/'
		},
		add_funding: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		fund_an_account: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_detail: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_detail_cih: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_igonigo: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		addformsreview: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		
		edit_funding_lao: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		docusign_confirmation: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		funding_confirmation: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		internal_funding_detail: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		msg_nigo_before: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		msg_nigo_after: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		}
	},
	sprint17: {
		themeUrl:{
			udThemeUrl: 		'/themes/ud2_atom/release_2016-07/',
			globalThemeUrl: '/assets/release_2016-07/'
		},
		add_funding: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		fund_an_account: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_detail: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_detail_cih: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_igonigo: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		addformsreview: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		
		edit_funding_lao: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		docusign_confirmation: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		funding_confirmation: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		internal_funding_detail: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		msg_nigo_before: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		msg_nigo_after: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		}, 
		non_gold:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
		igoforiwc:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
		msgnigo_before_direct_client:{
			 body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
		msgnigoiwc:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		}, 
		fundcancel:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		}, 
		
		addformsdirectclient:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		}
		
	}, 
	sprint18: {
		themeUrl:{
			udThemeUrl:'/themes/ud2_atom/release_2016-07/',
			globalThemeUrl:'/assets/release_2016-07/'
		},
		add_funding: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		fund_an_account: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_detail: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_detail_cih: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_igonigo: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		addformsreview: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		
		edit_funding_lao: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		docusign_confirmation: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		funding_confirmation: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		internal_funding_detail: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		msg_nigo_before: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		msg_nigo_after: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		}, 
		non_gold:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
		igoforiwc:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
		msgnigo_before_direct_client:{
			 body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
		msgnigoiwc:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		}, 
		fundcancel:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		}, 
		
		addformsdirectclient:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		}
		
	}, 
	sprint24: {
		themeUrl:{
			udThemeUrl:'/themes/ud2_atom/release_2016-07/',
			globalThemeUrl:'/assets/release_2016-07/'
		},
		add_funding: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		fund_an_account: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_detail: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_detail_cih: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_igonigo: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		addformsreview: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		
		edit_funding_lao: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		docusign_confirmation: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		funding_confirmation: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		internal_funding_detail: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		msg_nigo_before: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		msg_nigo_after: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		}, 
		non_gold:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
		igoforiwc:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
		msgnigo_before_direct_client:{
			 body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
		msgnigoiwc:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		}, 
		fundcancel:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		}, 
		
		addformsdirectclient:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		}
		
	},
	sprint26: {
		themeUrl:{
			udThemeUrl:'/themes/ud2_atom/release_2016-07/',
			globalThemeUrl:'/assets/release_2016-07/'
		},
		add_funding: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		fund_an_account: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_detail: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_detail_cih: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_igonigo: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		addformsreview: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		
		edit_funding_lao: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		docusign_confirmation: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		funding_confirmation: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		internal_funding_detail: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		msg_nigo_before: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		msg_nigo_after: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		}, 
		non_gold:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
		igoforiwc:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
		msgnigo_before_direct_client:{
			 body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
		msgnigoiwc:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		}, 
		fundcancel:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		}, 
		
		addformsdirectclient:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
			loa_noneditable:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		}
		
	},
	sprint27: {
		themeUrl:{
			udThemeUrl:'/themes/ud2_atom/release_2016-07/',
			globalThemeUrl:'/assets/release_2016-07/'
		},
		add_funding: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		fund_an_account: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_detail: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_detail_cih: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_igonigo: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		addformsreview: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		
		edit_funding_lao: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		docusign_confirmation: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		funding_confirmation: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		internal_funding_detail: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		msg_nigo_before: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		msg_nigo_after: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		}, 
		non_gold:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
		igoforiwc:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
		msgnigo_before_direct_client:{
			 body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
		msgnigoiwc:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		}, 
		fundcancel:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		}, 
		
		addformsdirectclient:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
			loa_noneditable:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
		fund_confirmation:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true 
		}
		
	},
	sprint29: {
		themeUrl:{
			udThemeUrl:'/themes/ud2_atom/release_2016-07/',
			globalThemeUrl:'/assets/release_2016-07/'
		},
		add_funding: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		
		release_fulfillment: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		fund_an_account: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_detail: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_detail_cih: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_igonigo: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		addformsreview: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		
		edit_funding_lao: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		docusign_confirmation: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		funding_confirmation: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		internal_funding_detail: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		msg_nigo_before: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		msg_nigo_after: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		}, 
		non_gold:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
		igoforiwc:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
		msgnigo_before_direct_client:{
			 body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
		msgnigoiwc:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		}, 
		fundcancel:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		}, 
		
		addformsdirectclient:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
			loa_noneditable:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
		fund_confirmation:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true 
		},
		unsolicited_forms:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
		funding_sources:{
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true 
		},
		suitability_details:{
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
			 
		}
		
		
	},
	sprint32: {
		themeUrl:{
			udThemeUrl:'/themes/ud2_atom/release_2016-07/',
			globalThemeUrl:'/assets/release_2016-07/'
		},
		add_funding: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		
			release_fulfillment: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		fund_an_account: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_detail: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_detail_cih: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		edit_funding_igonigo: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		addformsreview: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		
		edit_funding_lao: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		docusign_confirmation: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		funding_confirmation: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		internal_funding_detail: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		msg_nigo_before: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		msg_nigo_after: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		}, 
		non_gold:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
		igoforiwc:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
		msgnigo_before_direct_client:{
			 body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
		msgnigoiwc:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		}, 
		fundcancel:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		}, 
		
		addformsdirectclient:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
			loa_noneditable:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
		fund_confirmation:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true 
		},
		unsolicited_forms:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
		funding_sources:{
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true 
		},
		suitability_details:{
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
			 
		}
				
	},

	sprint33: {
		themeUrl:{
			udThemeUrl:'/themes/ud2_atom/release_2016-07/',
			globalThemeUrl:'/assets/release_2016-07/'
		},		
		loa_noneditable:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
		edit_funding_igonigo: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		}		
	},

	sprint36: {
		themeUrl:{
			udThemeUrl:'/themes/ud2_atom/release_2016-12/',
			globalThemeUrl:'/assets/release_2016-12/'
		},		
		loa_noneditable:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
		edit_funding_igonigo: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		funding_sources:{
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true 
		},
		suitability_details:{
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true
		},
		fund_confirmation:{
	    body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true 
		},
		igoforiwc:{
	    body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		},
		addformsdirectclient:{
	    body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true
		}
	},

	sprint39: {
		themeUrl:{
			udThemeUrl:'/themes/ud2_atom/release_2016-12/',
			globalThemeUrl:'/assets/release_2016-12/'
		},	

		add_funding: {
			body: true,
			left: false,
			popups: true,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			scenario: true
			
		},

		fund_an_account:{
			body: true,
			left: false, 
			pagetitle: true,
			proCSS: true,
			proJS: true,
			scenario: true
			
		},

		funding_confirmation: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			scenario: true
		 
		},

		loa_noneditable:{
	        body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true,
			scenario: true,
			popups: true
		},
		edit_funding_igonigo: {
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			scenario: true,
			popups: true
		},
		funding_sources:{
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			scenario: true,
			popups: true 

		},
		suitability_details:{
			body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			scenario: true,
			popups: true
		},
		fund_confirmation:{
	    body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			scenario: true,
			popups: true 
		},
		igoforiwc:{
	    body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true,
			scenario: true,
			popups: true
		},
		addformsdirectclient:{
	    body: true,
			left: false,
			pagetitle: true,
			proCSS: true,
			proJS: true,
			popups: true,
			scenario: true,
			popups: true
		}
	}
	

	// FEEL FREE TO UPDATE THE ABOVE CODE
};

// Updated the protoSettings
document.write('<script src="/themes/ud_atom/templates/proto_setting.js"></script>');

// Version based scenario 
(function(cont) {
	function checkstore() {
		if (typeof store === 'undefined') {
			setTimeout(function() {
			 	checkstore();
			}, 5);
		} else {
			$(document).trigger('storeLoaded');

			// If experience for UD2 is not defined then set a default                   
			if(store.get('release') === 'ud2' && store.get('experience') === undefined ){
				store.set('experience', 'experience1');
			}
		};
	};
	checkstore();
})(window);
document.write('<script src="'+TIAA_ud.localUrl+'js/ud_proj.js'+'"></script>');