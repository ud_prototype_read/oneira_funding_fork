$(document).on('bodyContLoaded', function(){
 
	// Core API for publishing
	$('form').each(function(){var _t = $(this);
		_t.find('input, button, select').on('click focus blur change keyup', function(e){ var _tt = $(this);
			$(document).trigger('elements.forms',[_t, _tt, e.type]);
		});
	});

	// Form field Subscribing layer for all events
	$(document).off('elements.forms').on('elements.forms', function(e, _form, _elem, _evtype){
		var _node 		= _elem.get(0).nodeName.toLowerCase(), 
		_elemtype     	= _elem.get(0).type,
		_elemName		= _elem.attr('name');

		// More control over the elements and events
		if(_node === 'input' && _elemtype === 'text' && (_evtype === 'keyup' || _evtype === 'focus')){
			if(isNull(_elem)){
				_elem.css('border', '1px solid red')
			}else{
				_elem.css('border', '1px solid green')
			}
		};

		if((_elemtype === 'radio' || _node === 'select') && _evtype === 'change'){
			console.log(_elem.val());
		};
 
	});

	//utilities
	function isNull(elem){ var _v = elem.val();
		if( _v === '' || _v === 'null' || _v === undefined ) {
			return true	
		}else{
			return false
		}
	};

	// This is already available in UD layout atom script - Here for reference	
	// var TIAA_ud = TIAA_ud || {};

	// // Detecting IE versions
	// TIAA_ud.ie = function (){
	//     if (window.ActiveXObject === undefined) return undefined;
	//     if (!window.XMLHttpRequest) return 6;
	//     if (!document.querySelector) return 7;
	//     if (!document.addEventListener) return 8;
	//     if (!window.atob) return 9;
	//     if (!document.__proto__) return 10;
	//     return 11;
	// };

	// window.ie = TIAA_ud.ie();

	/*======================================================
	=            myBob / iBob template specific            =
	======================================================*/
	// Enabing the tabs
	$('.myTab').tabs();

	if(location.href.indexOf('page2') > 0){

	// Delaying because to get the postion of the element. There is already a function which runs at 100ms 
	setTimeout(function(){
		(function($){
				// Get all the elements and their positions
			var $container = $('.bobContainer').eq(0),
				$leftCol   = $container.find('div.firstColumn'),
				$rightCol  = $container.find('div.secondColumn'),
				$ctrl      = $container.find('#layoutCtrlLink'),				
				
				// Get the postion of the elements
				contTop    = $container.offset().top,
				contLeft   = $container.offset().left,			
				leftTop    = $leftCol.offset().top,
				leftLeft   = $leftCol.offset().left,
				rightTop   = $rightCol.offset().top,
				rightLeft  = $rightCol.offset().left, 
				$scrollEle, scrollPos;
 
			// Setting the firstCol top postion for IE7
			if(ie <= 7){
				$leftCol.css('top', contTop+20);
			};

			// Setting controller top position
			$ctrl.css('top', contTop);


			// Setting the height of left Column
			$(window).on('resize', function(){
				$leftCol.height($(window).height() - (contTop+20));				
			});

			// Trigger the resize
			$(window).trigger('resize');

			// Enable the niceScroll for the left column
			$leftCol.niceScroll({
				horizrailenabled: false,
				autohidemode: false
			});

			// Nice Scroll for Table
			// $('div.tableWrapper').niceScroll();
              
            // <a href="#" id="layoutCtrlLink" class="minusLink expanded" data-expandTxt="Expand" data-collapseTxt="Collapse">Collapse</a> 
			$ctrl.on('click', function(e){ var _ = $(this);

				if(_.parent().hasClass('expanded')){
					console.log("YEs");
					_.parent().removeClass('expanded').addClass('collapsed');
					_.html(_.attr('data-expandTxt'));
					// hideLeft();
				}else if(_.parent().hasClass('collapsed')){
					console.log("Expnded");
					_.parent().removeClass('collapsed').addClass('expanded');
					_.html(_.attr('data-collapseTxt'));
				};

			});

			
			// Scrolling Borrowed from our own ud_main.js. 
			$(window).on('scroll',function(e){var _timer,
				_count = 10; 

				clearTimeout(_timer);
				_timer = setTimeout(function(){
					var _scrollAmt = $(document).scrollLeft();

					// Move the left comun along with left
					$leftCol.add($ctrl).css({ "left": -_scrollAmt+20 });

					// Move the slimScroll along with scroll
					$scrollEle = $('#ascrail2000');
					if($scrollEle.length > 0){
						scrollPos = parseInt($scrollEle.css('left'));
						$scrollEle.css("left", (-_scrollAmt+scrollPos) + '!important'); 						
					}; 
				},_count); 
			});

			
		})(jQuery);

	}, 500);
	};
/*-----  End of myBob / iBob template specific  ------*/
 


});