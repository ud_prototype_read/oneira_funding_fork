if ( typeof Object.create !== 'function' ) {
	Object.create = function( obj ) {
		function F() {};
		F.prototype = obj;
		return new F();
	};
}
(function( $, window, document, undefined ) {

	var multiSelect = {
		defaults:{
			selectAll: true,	//Select all option in the list true || false
			placeholder: 'Please Select...',	//Place-holder text for multi-select drop-down
			type: 'checkbox',	//Type of selections in multi-select drop-down checkbox || radio
			selectAllText: 'All items selected',	//Select all text		
			template: 'ul',	//Template type <ul> or <div>		
			minDisplay: 0,	//Display restiction to 1		
			selectAllOptions: false	//By default select all not checked	 true || false	
		},
		init: function(elem, options){ var self = this;	/* <------ Drop-down Initialization ------> */
			self.$elem = $(elem);
			self.options = $.extend({}, self.defaults, options);
			self.templateType();
			self.createWrapper();
		},
		templateType: function(elem, options){ var self = this;		/* <------ Drop-down type of options and template ------> */
			self.$checkType = (self.options.type === 'checkbox') ? true : false;	
			self.$templateType = (self.options.template === 'ul') ? true : false;									
		},
		createWrapper: function(elem, options){ var self = this;	/* <------ Drop-down options are wrapped with new styled div  ------> */
			self.$elem.hide();	
			self.$randno = Math.floor(100000 + Math.random() * 900000).toString().substring(0, 6);
			self.$elem.wrap('<div class="ms-select fll" rel="#ms-select-'+self.$randno+'" />');
			self.$msSelect  = self.$elem.closest('.ms-select');
			self.$msSelect.append('<span class="arrow"></span><div class="ms-placeholder">'+self.options.placeholder+'</div>');
			self.$msSelect.width(self.$elem.outerWidth());
			self.$list = $('<div class="ms-list closed" id="ms-select-'+self.$randno+'"><'+self.options.template+' class="ms-list-box lblSelectPair"></'+self.options.template+'></div>').width(self.$msSelect.outerWidth()-2);
			self.$list.appendTo('body');
			if(self.options.selectAll) {self.selectAll()}
			self.insertOptions();
			self.clickEvent();		
			self.bodyClick();								
			self.windowScroll();			
		},
		selectAll: function(elem, options){ var self = this;	/* <------ Drop-down select all list ------> */
			selectAllName = 'ms_select_all'+self.$randno;
			if(self.$checkType){
				(self.$templateType) ? self.$list.children().prepend('<li class="ms-list-items"><input type="'+self.options.type+'" class="selectAll" id="'+selectAllName+'" /><label for="'+selectAllName+'">Select All</label></li>') : self.$list.children().prepend('<div class="ms-list-items"><input type="'+self.options.type+'" class="selectAll" id="'+selectAllName+'" /><label for="'+selectAllName+'">Select All</label></div>');
			}			
		},
		insertOptions: function(elem, options){ var self = this;	/* <------ Drop-down insert options to the list ------> */
			self.$listBoxId = $(self.$elem.closest('div').attr('rel'));
			count = 0;
			selectedName = 'ms_select'+self.$randno;
			self.$elem.find('option').each(function(){ var _options = $(this);
				count++;
				$selectedName = (self.$checkType) ? selectedName+'_'+count : selectedName;
				(self.$templateType) ? self.$listBoxId.children().append('<li class="ms-list-items ms-list-options"><input type="'+self.options.type+'" class="ms-input" name="'+$selectedName+'" id="'+$selectedName+'" /><label for="'+$selectedName+'">'+_options.text()+'</label></li>') : self.$listBoxId.children().append('<div class="ms-list-items ms-list-options"><input type="'+self.options.type+'" class="ms-input" name="'+$selectedName+'" id="'+$selectedName+'" /><label for="'+$selectedName+'">'+_options.text()+'</label></div>');
			});
			self.selectAllOptions();
		},
		selectAllOptions: function(elem, options){ var self = this;	/* <------ Drop-down by default select all list ------> */
			self.$listSelectAllId = $(self.$elem.closest('div').attr('rel'));
			if(self.options.selectAllOptions){
				self.$elem.closest('div').find('.ms-placeholder').text(self.options.selectAllText);
				self.$listSelectAllId.find('input').attr('checked', true);
			}
		},
		clickEvent: function(elem, options){ var self = this;	/* <------ Click event on drop-down multi-select  ------> */
			self.$msSelect.on('click',function(e){	var _ = $(this),
				elemLeft = self.$msSelect.offset().left;
				$('.ms-list').addClass('closed');
				self.$listId = $(_.attr('rel'));
				self.$listId.css('left', elemLeft+'px').toggleClass('closed');
				self.$listwrap = $(self.$listId);	
				self.selectItems();
				self.positionListbox();
				self.resizeWindow();
			});					
		},
		selectItems :  function(elem, options){ var self = this;		/* <------ Listing out selected elements  ------> */
			var $checked 	   = 0,
				$element 	   = self.$list.find('.ms-list-items'),
				$elementInput  = $element.find('input');
			$elementInput.on('click',function(){ var _ = $(this);
				var $listId        = _.closest('.ms-list'),			
					$displayItem   = self.$elem.closest('.ms-select').find('.ms-placeholder'),
					$listSet	   = $listId.find('.ms-list-items').children('input.ms-input'),
					$totalInput	   = $listSet.length;
					$checked       = $elementInput.filter(':checked').not('.selectAll').length;
				if(self.options.selectAll && self.$checkType){						
					var $selectAll = _.hasClass('selectAll');
					if ($selectAll && _.is(':checked')){	//Select All Checkboxes 
						$listSet.attr('checked',true);
						$checked = $totalInput;
					}else if($selectAll && _.not(':checked')){	//Unselect All Checkboxes
						$checked = 0;
						$listSet.attr('checked',false);
					}else if($totalInput === $checked){	 //Individual Select All Checkboxes 						 
						$elementInput.eq(0).attr('checked',true);
					}else if($totalInput != $checked){	//Individual Unselect All Checkboxes
						$elementInput.eq(0).attr('checked',false);
					}					
				}
				if($checked === self.options.minDisplay){	//Only one selection 
					$displayItem.text($element.children('input.ms-input:checked').siblings('label').text());
				}else if($checked > self.options.minDisplay && ($checked < $totalInput)){	//More then one selection 
					$displayItem.text($checked +' of '+$totalInput+' selected');
				}else if($totalInput === $checked){		//All selection
					$displayItem.text(self.options.selectAllText);
				}else{
					$displayItem.text(self.options.placeholder);	//No selection
				}						
			});					
		},
		bodyClick : function(elem, options){ var self = this;		/* <------ Hide Drop-down on body click  ------> */		
			$(document).add('.hd').on('click',function (e){
				if (($(e.target).closest('.arrow').length === 0) && ($(e.target).closest('.ms-placeholder').length === 0) && $(e.target).closest('.ms-list').length === 0){
					$('.ms-list').addClass('closed');						
				}									
			});				
		},
		windowScroll : function(elem, options){ var self = this;		/* <------ Hide Drop-down on page scroll  ------> */		
			$('#wrapper').scroll(function (e){
				$('.ms-list').addClass('closed');	
			});				
		},
		resizeWindow : function(elem, options){ var self = this;		/* <------ Adjust drop-down position on window resize  ------> */	
			$(window).resize(function(){
				self.positionListbox();
			});		
		},
		positionListbox : function(elem, options){ var self = this;		/* <------ Adjust drop-down position  ------> */		
			self.$windowHeight = $(window).height();
			self.$elemSelectTop = self.$msSelect.offset().top;
			self.$elemSelectHeight = self.$msSelect.outerHeight();
			self.$elemDDHeight = self.$listwrap.height();
			(self.$windowHeight < (self.$elemSelectTop + self.$elemDDHeight)) ? self.$listwrap.css('top', (self.$elemSelectTop - self.$elemDDHeight)+'px') : self.$listwrap.css('top', (self.$elemSelectTop + (self.$elemSelectHeight-1))+'px');	
		}				
	};
	$.fn.multipleSelect =  function(options){
		this.each(function(i){
			var multiselect = Object.create(multiSelect);
			multiselect.init($(this), options);			
		});	
		return this;			
	};		
})( jQuery, window, document );